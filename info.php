<?php

// turing machine

$states = [1, 2, 3, 4, 5, 6];   // Number of states
$accept_states = [6];           // Final State

// state => ['read_condition' => ['write_value', 'move_direction', 'new_state'];

$rules = [
    1 => ['1' => ['a', 'r', 2],
          '+' => ['_', 'r', 6]],
    2 => ['1' => ['1', 'r', 2],
          '+' => ['+', 'r', 3]],
];

// input tape

$tape = ['1','1'];
$position = 0;      // the position of the tape starts. Remember that array is start from 0; 
$state = 1;         // Initial State

// The Process

phpinfo();
